let botStateDefinition = {
  botState: {
    position: {
      x: 0,
      y: 13.5,
      t: 0
    },
    currentZones: [],
    previousZones: [],
    mobility: false,
    cones: 0,
    cubes: 0,
    charging: false,
    parked: false,
    docked: false,
    engaged: false,
    contact: false,
    lastContact: 0
  },
  drawnElements: [],
  init: '',
  update: ''
};

export default botStateDefinition;
