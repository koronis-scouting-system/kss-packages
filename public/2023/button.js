let buttonDefinitions = [
  {
    id: 0,
    name: 'pickup_cone',
    title: 'Cone',
    group: 'cone',
    horizontalWeight: 0.7,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Yellow', outline: 'Dark Yellow', text: 'Dark Yellow'},
      depressed: {palette: 'button', fill: 'Yellow', outline: 'Dark Yellow', text: 'Dark Yellow'}
    }
  },
  {
    id: 1,
    name: 'drop_cone',
    title: 'Drop',
    group: 'cone',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  },
  {
    id: 2,
    name: 'pickup_cube',
    title: 'Cube',
    group: 'cube',
    horizontalWeight: 0.7,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Purple', outline: 'Dark Purple', text: 'Dark Purple'},
      depressed: {palette: 'button', fill: 'Purple', outline: 'Dark Purple', text: 'Dark Purple'}

    }
  },
  {
    id: 3,
    name: 'drop_cube',
    title: 'Drop',
    group: 'cube',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  },
  {
    id: 4,
    name: 'score_top_row_cone',
    title: 'Top Cone',
    group: 'top_row',
    horizontalWeight: 0.4,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Yellow', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Yellow', text: 'Blue'}
    }
  },
  {
    id: 5,
    name: 'score_top_row_cube',
    title: 'Cube',
    group: 'top_row',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Purple', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Purple', text: 'Blue'}
    }
  },
  {
    id: 6,
    name: 'fail_top_row',
    title: 'Fail',
    group: 'top_row',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  },
  {
    id: 7,
    name: 'score_middle_row_cone',
    title: 'Middle Cone',
    group: 'middle_row',
    horizontalWeight: 0.4,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Blue', outline: 'Dark Yellow', text: 'Dark Blue'},
      depressed: {palette: 'button', fill: 'Blue', outline: 'Dark Yellow', text: 'Dark Blue'}
    }
  },
  {
    id: 8,
    name: 'score_middle_row_cube',
    title: 'Cube',
    group: 'middle_row',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Dark Purple', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Dark Purple', text: 'Blue'}
    }
  },
  {
    id: 9,
    name: 'fail_middle_row',
    title: 'Fail',
    group: 'middle_row',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  },
  {
    id: 10,
    name: 'score_bottom_row_cone',
    title: 'J Bottom Cone',
    group: 'bottom_row',
    horizontalWeight: 0.4,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Yellow', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Yellow', text: 'Blue'}
    }
  },
  {
    id: 11,
    name: 'score_bottom_row_cube',
    title: 'Q Cube',
    group: 'bottom_row',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Purple', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Purple', text: 'Blue'}
    }
  },
  {
    id: 12,
    name: 'fail_bottom_row',
    title: 'Fail',
    group: 'bottom_row',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  },
  {
    id: 13,
    name: 'score_link',
    title: 'Link',
    group: 'link',
    horizontalWeight: 0.5,
    verticalWeight: 1,
    watcher: 'return true;',
    style: {
      released: {palette: 'button', fill: 'Light Green', outline: 'Dark Green', text: 'Dark Green'},
      depressed: {palette: 'button', fill: 'Green', outline: 'Dark Green', text: 'Dark Green'}
    }
  },
  {
    id: 14,
    name: 'start_charge',
    title: 'Start Charge',
    group: 'charge1',
    horizontalWeight: 1,
    verticalWeight: 1,
    watcher: 'return (!bS.charging && bS.currentZones.findIndex((e) => {return (e.name == \'charge_station\' && e.isAllied);}) != -1);',
    style: {
      released: {palette: 'button', fill: 'Light Green', outline: 'Dark Green', text: 'Dark Green'},
      depressed: {palette: 'button', fill: 'Green', outline: 'Dark Green', text: 'Dark Green'}
    }
  },
  {
    id: 15,
    name: 'parked',
    title: 'Park',
    group: 'charge1',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return bS.charging;',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Blue', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Blue', text: 'Blue'}
    }
  },
  {
    id: 16,
    name: 'docked',
    title: 'Dock',
    group: 'charge1',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return bS.charging;',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Blue', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Blue', text: 'Blue'}
    }
  },
  {
    id: 17,
    name: 'fail_charge',
    title: 'W Fail',
    group: 'charge1',
    horizontalWeight: 0.3,
    verticalWeight: 1,
    watcher: 'return bS.charging;',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  },
  {
    id: 18,
    name: 'engaged',
    title: 'Engage',
    group: 'charge2',
    horizontalWeight: 0.5,
    verticalWeight: 1,
    watcher: 'return bS.charging && bS.docked;',
    style: {
      released: {palette: 'button', fill: 'Light Green', outline: 'Dark Green', text: 'Dark Green'},
      depressed: {palette: 'button', fill: 'Green', outline: 'Dark Green', text: 'Dark Green'}
    }
  },
  {
    id: 19,
    name: 'fail_engaged',
    title: 'X Fail',
    group: 'charge2',
    horizontalWeight: 0.5,
    verticalWeight: 1,
    watcher: 'return bS.charging && bS.docked;',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  },
  {
    id: 20,
    name: 'start_pin',
    title: 'Start Pin',
    group: 'pin',
    horizontalWeight: 0.5,
    verticalWeight: 1,
    watcher: 'return (!bS.contact);',
    style: {
      released: {palette: 'button', fill: 'Very Light Blue', outline: 'Blue', text: 'Blue'},
      depressed: {palette: 'button', fill: 'Light Blue', outline: 'Blue', text: 'Blue'}
    }
  },
  {
    id: 21,
    name: 'receive_pin',
    title: 'Received Pin',
    group: 'pin',
    horizontalWeight: 0.5,
    verticalWeight: 1,
    watcher: 'return (!bS.contact);',
    style: {
      released: {palette: 'button', fill: 'Light Blue', outline: 'Dark Blue', text: 'Dark Blue'},
      depressed: {palette: 'button', fill: 'Blue', outline: 'Dark Blue', text: 'Dark Blue'}
    }
  },
  {
    id: 22,
    name: 'stop_pin',
    title: 'Y Pin Stopped',
    group: 'pin',
    horizontalWeight: 1,
    verticalWeight: 1,
    watcher: 'return (bS.contact);',
    style: {
      released: {palette: 'button', fill: 'Light Red', outline: 'Dark Red', text: 'Dark Red'},
      depressed: {palette: 'button', fill: 'Red', outline: 'Dark Red', text: 'Dark Red'}
    }
  }
];

export default buttonDefinitions;
