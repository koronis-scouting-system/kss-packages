let eventDefinitions = [
  {
    id: 0,
    name: 'pickup_cone',
    title: 'Picked Up Cone',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'pickup_cone\')}) != -1);',
    emitter: 'bS.cones++; return {};'
  },
  {
    id: 1,
    name: 'drop_cone',
    title: 'Dropped Cone',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'drop_cone\')}) != -1);',
    emitter: 'bS.cones--; return {};'
  },
  {
    id: 2,
    name: 'pickup_cube',
    title: 'Picked Up Cube',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'pickup_cube\')}) != -1);',
    emitter: 'bS.cubes++; return {};'
  },
  {
    id: 3,
    name: 'drop_cube',
    title: 'Dropped Cube',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'drop_cube\')}) != -1);',
    emitter: 'bS.cubes--; return {};'
  },
  {
    id: 4,
    name: 'score_cone',
    title: 'Scored Cone',
    variables: {
      row: 'top' //top, middle, or bottom
    },
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'score\') && e.name.includes(\'cone\');}) != -1);',
    emitter: 'bS.cones--;' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'top\')}) != -1)){return {row: \'top\'};}' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'middle\')}) != -1)){return {row: \'middle\'};}' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'bottom\')}) != -1)){return {row: \'bottom\'};}'
  },
  {
    id: 5,
    name: 'score_cube',
    title: 'Scored Cube',
    variables: {
      row: 'top' //top, middle, or bottom
    },
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'score\') && e.name.includes(\'cube\');}) != -1);',
    emitter: 'bS.cubes--;' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'top\')}) != -1)){return {row: \'top\'};}' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'middle\')}) != -1)){return {row: \'middle\'};}' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'bottom\')}) != -1)){return {row: \'bottom\'};}'
  },
  {
    id: 6,
    name: 'fail_row',
    title: 'Failed to Score',
    variables: {
      row: 'top' //top, middle, or bottom
    },
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'fail\') && e.name.includes(\'row\');}) != -1);',
    emitter: 'if((btnS.findIndex((e) => {return e.name.includes(\'top\')}) != -1)){return {row: \'top\'};}' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'middle\')}) != -1)){return {row: \'middle\'};}' +
    'if((btnS.findIndex((e) => {return e.name.includes(\'bottom\')}) != -1)){return {row: \'bottom\'};}'
  },
  {
    id: 7,
    name: 'score_link',
    title: 'Scored Link',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'score\') && e.name.includes(\'link\');}) != -1);',
    emitter: 'return {};'
  },
  {
    id: 8,
    name: 'start_charge',
    title: 'Charge Started',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'start_charge\')}) != -1);',
    emitter: 'bS.charging = true; bS.parked = false; bS.docked = false; bS.engaged = false; return {};'
  },
  {
    id: 9,
    name: 'parked',
    title: 'Parked',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'parked\')}) != -1);',
    emitter: 'bS.charging = true; bS.parked = true; bS.docked = false; bS.engaged = false; return {};'
  },
  {
    id: 10,
    name: 'docked',
    title: 'Docked',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'docked\')}) != -1);',
    emitter: 'bS.charging = true; bS.parked = false; bS.docked = true; return {};'
  },
  {
    id: 11,
    name: 'fail_charge',
    title: 'Charge Failed',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'fail_charge\')}) != -1);',
    emitter: 'bS.charging = false; bS.parked = false; bS.docked = false; bS.engaged = false; return {};'
  },
  {
    id: 12,
    name: 'engaged',
    title: 'Engaged',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name === \'engaged\'}) != -1);',
    emitter: 'bS.engaged = true; return {};'
  },
  {
    id: 13,
    name: 'fail_engaged',
    title: 'Failed Engaged',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name === \'fail_engaged\'}) != -1);',
    emitter: 'bS.engaged = false; return {};'
  },
  {
    id: 14,
    name: 'start_pin',
    title: 'Pinning another Robot',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'start_pin\')}) != -1);',
    emitter: 'bS.contact = true; bS.lastContact = bS.position.t; return {};'
  },
  {
    id: 15,
    name: 'receive_pin',
    title: 'Pinned by another Robot',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'receive_pin\')}) != -1);',
    emitter: 'bS.contact = true; bS.lastContact = bS.position.t; return {};'
  },
  {
    id: 16,
    name: 'stop_pin',
    title: 'Pin Stopped',
    variables: {},
    watcher: 'return (btnS.findIndex((e) => {return e.name.includes(\'stop_pin\')}) != -1) ||' +
    '(bS.contact && bS.lastContact + 5 < bS.position.t);',
    emitter: 'bS.contact = false; bS.lastContact = 0; return {};'
  },
  {
    id: 17,
    name: 'mobility',
    title: 'Mobility',
    variables: {},
    watcher: 'return (!bS.mobility && ' + 
    'bS.previousZones.findIndex((e) => {return (e.name == \'community\' && e.isAllied);}) != -1 && ' +
    'bS.currentZones.findIndex((e) => {return (e.name == \'community\' && e.isAllied);}) == -1 && ' +
    'bS.position.t > 0);',
    emitter: 'bS.mobility = true; return {};'
  }
];

export default eventDefinitions;
