let fieldStateDefinition = {
  fieldState: {
    dimensions: {
      x: 54,
      y: 27
    },
    zones: [
      {
        id: 0,
        name: 'half_field',
        isAllied: true,
        size: {x: 27, y: 27},
        position: {x: 0,y: 0}
      },
      {
        id: 1,
        name: 'loading_zone',
        isAllied: false,
        size: {x: 0, y: 0},
        position: {x: 0, y: 0}
      },
      {
        id: 2,
        name: 'community',
        isAllied: true,
        size: {x: 0, y: 0},
        position: {x: 0, y: 0},
        points: [
          {x:38,y:27},
          {x:38,y:14},
          {x:43,y:14},
          {x:43,y:9},
          {x:54,y:9},
          {x:54,y:27}
        ]
      },
      {
        id: 3,
        name: 'charge_station',
        isAllied: true,
        size: {x: 8, y: 10},
        position: {x: 37, y: 13}
      }
    ],
    inverted: {
      x: false,
      y: false
    }
  },
  drawnElements: [
    {
      id: 0,
      name: 'half_field',
      isAllied: true,
      size: {x: 27, y: 27},
      position: {x: 0, y: 0},
      style: {
        palette: 'field',
        fill: 'Transparent',
        outline: 'Dark Grey'
      }
    },
    {
      id: 1,
      name: 'grid',
      isAllied: true,
      size: {x: 5, y: 18},
      position: {x: 49, y: 9},
      style: {
        palette: 'red',
        fill: 'Light Team Color',
        outline: 'Dark Team Color'
      }
    },
    {
      id: 2,
      name: 'loading_zone',
      isAllied: false,
      size: {x: 0, y: 0},
      position: {x: 0, y: 0},
      points: [
        {x:32,y:0},
        {x:54,y:0},
        {x:54,y:9},
        {x:43,y:9},
        {x:43,y:5},
        {x:32,y:5}
      ],
      style: {
        palette: 'blue',
        fill: 'Transparent',
        outline: 'Dark Team Color'
      }
    },
    {
      id: 3,
      name: 'community',
      isAllied: true,
      size: {x: 0, y: 0},
      position: {x: 0, y: 0},
      points: [
        {x:38,y:27},
        {x:38,y:14},
        {x:43,y:14},
        {x:43,y:9},
        {x:54,y:9},
        {x:54,y:27}
      ],
      style: {
        palette: 'red',
        fill: 'Transparent',
        outline: 'Dark Team Color'
      }
    },
    {
      id: 4,
      name: 'charge_station',
      isAllied: true,
      size: {x: 6, y: 8},
      position: {x: 38, y: 14},
      style: {
        palette: 'field',
        fill: 'White',
        outline: 'Black'
      }
    }
  ],
  init: ( //Mirror both zones and drawn elements across the y axis in the middle
    'var newZones = [];' +
    'var len = fS.zones.length;' +
    'for(var i = 0;i < len;i++) {' +
    '  var currZone = Object.assign({},fS.zones[i]);' +
    '  currZone.position = Object.assign({},fS.zones[i].position);' +
    '  currZone.size = Object.assign({},fS.zones[i].size);' +
    '  currZone.style = Object.assign({},fS.zones[i].style);' +
    '  currZone.position.x = fS.dimensions.x - (fS.zones[i].size.x + fS.zones[i].position.x);' +
    '  currZone.id = i + fS.zones.length;' +
    '  currZone.isAllied = !fS.zones[i].isAllied;' +
    '  if(typeof currZone.points != \'undefined\') {' +
    '    currZone.points = fS.zones[i].points.slice();' +
    '    for(var j = 0;j < currZone.points.length;j++) {' +
    '      currZone.points[j] = Object.assign({}, fS.zones[i].points[j]);' +
    '      currZone.points[j].x = fS.dimensions.x - fS.zones[i].points[j].x;' +
    '    }' +
    '  }' +
    '  newZones.push(currZone);' +
    '}' +
    'for(var i = 0;i < newZones.length;i++) { fS.zones.push(newZones[i]); }' +
    'var newDrawnElements = [];' +
    'len = dE.length;' +
    'for(var i = 1;i < len;i++) {' +
    '  var currDrawnElement = Object.assign({}, dE[i]);' +
    '  currDrawnElement.position = Object.assign({}, dE[i].position);' +
    '  currDrawnElement.size = Object.assign({}, dE[i].size);' +
    '  currDrawnElement.style = Object.assign({}, dE[i].style);' +
    '  currDrawnElement.position.x = fS.dimensions.x - (dE[i].size.x + dE[i].position.x);' +
    '  currDrawnElement.id = i + dE.length;' +
    '  currDrawnElement.isAllied = !dE[i].isAllied;' +
    '  if(typeof currDrawnElement.points != \'undefined\') {' +
    '    currDrawnElement.points = dE[i].points.slice();' +
    '    for(var j = 0;j < currDrawnElement.points.length;j++) {' +
    '      currDrawnElement.points[j] = Object.assign({}, dE[i].points[j]);' +
    '      currDrawnElement.points[j].x = fS.dimensions.x - dE[i].points[j].x;' +
    '    }' +
    '  }' +
    '  if(currDrawnElement.style.palette == \'red\') {currDrawnElement.style.palette = \'blue\';}' +
    '  else if(currDrawnElement.style.palette == \'blue\') {currDrawnElement.style.palette = \'red\';}' +
    '  newDrawnElements.push(currDrawnElement);' +
    '}' +
    'for(var i = 0;i < newDrawnElements.length;i++) { dE.push(newDrawnElements[i]); }'
  ),  
  update: ''
};

export default fieldStateDefinition;
