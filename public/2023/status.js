let statusUpdateDefinition = {
  update: 'let climbLevel = bS.engaged ? \'Engaged\' : bS.docked ? \'Docked\' : bS.parked ? \'Parked\' : bS.charging ? \'Started\' : \'None\';' +
  'return [' +
    '{title: (\'Cones: \' + bS.cones), style: {palette: \'button\', fill: \'White\', outline: \'Dark Yellow\', text: \'Black\'}},' +
    '{title: (\'Cubes: \' + bS.cubes), style: {palette: \'button\', fill: \'White\', outline: \'Dark Purple\', text: \'Black\'}},' +
    '{title: (\'Charge: \' + climbLevel), style: {palette: \'button\', fill: \'White\', outline: \'Green\', text: \'Black\'}}' +
  '];'
};

export default statusUpdateDefinition;
